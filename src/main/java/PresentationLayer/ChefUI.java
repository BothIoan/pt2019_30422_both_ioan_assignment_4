package PresentationLayer;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.MenuItem;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

public class ChefUI extends JFrame implements Observer{
    JTable table;
    Restaurant restaurant;
    ArrayList<Order>orders;
    public ChefUI(Restaurant restaurant){
        this.restaurant=restaurant;
        setLayout(new GridLayout(1,1));
        setSize(500,500);
        setTitle("Chef");
        setLocationRelativeTo(null);
        JScrollPane sPane= new JScrollPane(table);
        add(sPane);
        setVisible(true);
    }
    public String[]collumngen(int orderSize){
        String[] aux = new String[orderSize];
        for(int i = 1 ;i<= orderSize;i++){
            aux[i-1]= "Item "+ i;
        }
        return aux;
    }
    public String[][] rowsgen(ArrayList<Order> orders, int orderSize){
        int i=0;
        int j=0;
        String[][]aux= new String[orders.size()][orderSize];
        for(Order order:orders){
            for(MenuItem item:order.getOrderedItems()){
                aux[i][j]=item.getName();
                j++;
            }
            i++;
            j=0;
        }
        return aux;
    }
    public void update(){ new ErrorJframe("New Order!");
    }

}
